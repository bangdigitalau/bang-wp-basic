<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
$domain = $_SERVER['HTTP_HOST'];
$domain = str_replace('www.', '', $domain);

//dynamic database settings
if (strpos($domain, 'staging') !== false) {
    //on staging server
    define('DB_HOST', 'localhost');
} else {
    define('DB_HOST', 'ldbserver');
}
define('DB_NAME', substr($domain, 0, strpos($domain, '.')) . '_wp');
define('DB_USER', 'bang_wp_user');
define('DB_PASSWORD', 's3apt2mwqr');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');


if (isset($_SERVER['HTTP_X_SSL']) or ( isset($_SERVER['HTTP_X_FORWARDED_PROTO']) and $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'))
    $_SERVER['HTTPS'] = 'on';
$proto = (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';

//url settings
define('WP_SITEURL', $proto . '://' . $_SERVER['HTTP_HOST']);
define('WP_HOME', $proto . '://' . $_SERVER['HTTP_HOST']);

/* * #@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'ln|B/p|;y;)?NcSk@g@Z/f<5I-vU/hR<f4zC[s0-rc69!|;%|;U-7YHJ_x%][Hy-');
define('SECURE_AUTH_KEY', '!WCo<MskCj. A{3DKF1GrAm{Au_(f>p.Z]H@7-b88|Rrn0.U|aqA6[xYL&:cPe%@');
define('LOGGED_IN_KEY', 'wQ&s^kyl}7a76*Uxit&W}syfJYWdW~NvCA87iJ8~I|#>.;(1A=,j2U2Qbcl&|-<j');
define('NONCE_KEY', '/1Um5N|tnAP=yx@O=D)en[GU;XnsB3r|FW|&B2(8Moo@|MVMw(>wCtqv~%Y2 U>!');
define('AUTH_SALT', 'b(~$Dw>JP0_ddFyH(v#B~ULG-I^U#ZpXCYc+ui:GUC;YyBkF(TvVFrGa!Yw`M&F+');
define('SECURE_AUTH_SALT', 'f1@~/ZV0|2hB~h9KZ0V^`*e-|}YevHU|o!g_4MI)jZA/DoD|5Jd?nda2+{hXOi-8');
define('LOGGED_IN_SALT', '|_I4FO@&?i+b8y;=z$<-3icY33*,2#{`..n_lB4=b?RN<j|wA(g{h.V|1eR/EO,^');
define('NONCE_SALT', 'bku~6C<p=A 8+c;jCgX)hYgr17%So:}Y[6[s<_N^M5+t+g)vE{<x!ir] IY]ndyf');

/* * #@- */

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH'))
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
