<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$footer_logo = get_field('footer_logo', 'option');
$footer_logo_url = wp_get_attachment_image_src($footer_logo, 'full')[0];
$footer_text = get_field('footer_text', 'option');
?>
    <div class="prefooter d-none d-md-block">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <a href="<?php echo get_home_url(); ?>" title="<?php echo get_bloginfo('name')?>"><img src="<?php echo $footer_logo_url; ?>"></a>
                </div>
                <div class="col-md-9">
                <?php if( have_rows('membership_logo', 'option') ): ?>
                <div class="prefooter-membership">
                    <?php while ( have_rows('membership_logo', 'option') ) : the_row(); ?>
                        <?php
                            $membership_logo_image = get_sub_field('membership_logo_image', 'option');
                            $membership_logo_image_url = wp_get_attachment_image_src($membership_logo_image,'full')[0];
                            $membership_logo_url = get_sub_field('membership_logo_url', 'option');
                        ?>
                        <div class="prefooter-membership-items">
                            <?php if($membership_logo_url) : ?><a href="<?php echo $membership_logo_url; ?>" target="_blank"><?php endif; ?>
                            <img src="<?php echo $membership_logo_image_url; ?>">
                            <?php if($membership_logo_url) : ?></a><?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container d-none d-md-block">
        <div class="row">
            <div class="col-12">
                <div class="hr"></div>
            </div>
        </div>
    </div>

    <div class="prefooter-mobile d-block d-md-none">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="prefooter-mobile-logo" href="<?php echo get_home_url(); ?>" title="<?php echo get_bloginfo('name')?>"><img src="<?php echo $footer_logo_url; ?>"></a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="container">
            <div class="row">
                <?php if ( is_active_sidebar( 'footer-widget-1' ) ) : ?>
                <div class="col-12 col-md-2">
                    <div class="footer-menu footer-widget">
                        <?php dynamic_sidebar('footer-widget-1'); ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php if ( is_active_sidebar( 'footer-widget-2' ) ) : ?>
                <div class="col-12 col-md-2">
                    <div class="footer-menu footer-widget">
                        <?php dynamic_sidebar('footer-widget-2'); ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php if ( is_active_sidebar( 'footer-widget-3' ) ) : ?>
                <div class="col-12 col-md-2">
                    <div class="footer-menu footer-widget">
                        <?php dynamic_sidebar('footer-widget-3'); ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php if ( is_active_sidebar( 'footer-widget-4' ) ) : ?>
                <div class="col-12 col-md-2">
                    <div class="footer-menu footer-widget">
                        <?php dynamic_sidebar('footer-widget-4'); ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php if ( is_active_sidebar( 'footer-widget-5' ) ) : ?>
                <div class="col-12 col-md-2">
                    <div class="footer-menu footer-widget">
                        <?php dynamic_sidebar('footer-widget-5'); ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php if ( is_active_sidebar( 'footer-widget-6' ) ) : ?>
                <div class="col-12 col-md-2">
                    <div class="footer-menu footer-widget footer-details">
                        <?php dynamic_sidebar('footer-widget-6'); ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="legal">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php echo $footer_text; ?>
                </div>
            </div>
        </div>
    </div>

</div><!-- /main-wrap -->
<?php
    $sticky_footer_text = get_field('sticky_footer_text', 'option');
    $sticky_footer_url = get_field('sticky_footer_url', 'option');
?>
<?php if ($sticky_footer_url) : ?>
<div class="sticky-footer d-block d-md-none">
    <a href="<?php echo $sticky_footer_url; ?>" class="btn"><?php echo $sticky_footer_text; ?></a>
</div>
<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>
