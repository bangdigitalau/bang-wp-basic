<?php

/**
 * Bang_Shortcode
 * Shortcode Init and Handlers
 *
 * @author Ricky
 */
class Bang_Shortcodes {

    /**
     * Hook in shortcode handlers.
     */
    public static function init() {
        self::add_shortcodes();
    }

    /**
     * Add Shortcodes
     */
    static function add_shortcodes() {
        // bang_SHORTCODE
        $shortcodes = array();

        foreach ($shortcodes as $shortcode) {
            add_shortcode('bang_' . $shortcode, array(__CLASS__, $shortcode));
        }
    }

}

Bang_Shortcodes::init();
