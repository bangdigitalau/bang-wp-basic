<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<div id="primary" class="content-area blog-content main-content-area">
	<main id="main" class="site-main" role="main">

		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<?php get_template_part( 'template-parts/heading', 'page' ); ?>

					<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

						<div class="row" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="col-12">
								<div class="row">
									<div class="col-12">
										<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<?php get_template_part('post','meta'); ?>
									</div>
								</div>
								<div class="row">
									<?php if(has_post_thumbnail()) { ?>
									<div class="col-md-5 post-image">
										<?php the_post_thumbnail('medium', array('class' => '')); //article-featured ?>
									</div>
									<?php } ?>
									<div class="col-md-<?php if(has_post_thumbnail()) { ?>7<?php } else { ?>12<?php } ?> post-content">
										<p><?php echo wp_trim_words( get_the_content(), 50 ); ?></p>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<div class="post-btn blog-btn">
											<a href="<?php the_permalink(); ?>" class="btn darkblue-bg white-txt">Continue reading</a>
										</div>
									</div>
								</div>
							</div>
						</div>

					<?php if (($wp_query->current_post + 1) < ($wp_query->post_count)) {echo '<div class="post-divider"></div>';}?>

					<?php endwhile; ?>

					<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) : ?>
					<div class="post-navigation">
						<?php
						global $wp_query;

						$big = 999999999; // need an unlikely integer

						echo paginate_links( array(
							'base'    => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
							'format'  => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'total'   => $wp_query->max_num_pages
						) );
						?>
					</div>
					<?php endif; ?>

					<?php endif; ?>
				</div>
				<div class="col-md-4 d-none d-md-block">
					<?php get_sidebar('blog'); ?>
				</div>
			</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>
