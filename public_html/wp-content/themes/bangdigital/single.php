<?php get_header(); ?>
<div id="primary" class="content-area blog-content main-content-area">
	<main id="main" class="site-main" role="main">

		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<?php get_template_part( 'template-parts/heading', 'page' ); ?>

						<?php get_template_part('post','meta'); ?>

			            <?php if(has_post_thumbnail()){ ?>
							<?php the_post_thumbnail('full', array('class' => 'aligncenter')); ?>
						<?php } ?>

			           	<?php the_content(); ?>

						<?php //get_template_part('post','tax'); ?>

					</div><!-- /post -->

					<?php endwhile; ?><?php endif; ?>
				</div>
				<div class="col-md-4 d-none d-md-block">
					<?php get_sidebar('blog'); ?>
				</div>
			</div>
		</div>

	</main>
</div>

<?php get_footer(); ?>
