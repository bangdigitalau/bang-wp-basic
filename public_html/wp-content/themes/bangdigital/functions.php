<?php
/**
 * Theme functions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 */
include_once 'includes/class-bang-theme-setup.php';

// Add support for custom styles in WordPress editor
add_editor_style();

// Remove width and height dimensions from uploaded images
add_filter( 'get_image_tag', 'remove_width_and_height_attribute', 10 );
add_filter( 'post_thumbnail_html', 'remove_width_and_height_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_and_height_attribute', 10 );
function remove_width_and_height_attribute( $html ) {
    return preg_replace( '/(height|width)="\d*"\s/', "", $html );
}

// Remove related YouTube videos
add_filter('oembed_dataparse','sp_strip_related_videos', 10, 3);
add_filter('embed_handler_html', 'sp_custom_youtube_settings');
add_filter('embed_oembed_html', 'sp_custom_youtube_settings');
function sp_strip_related_videos($return, $data, $url) {
    if ($data->provider_name == 'YouTube') {
        $data->html = str_replace('feature=oembed', 'feature=oembed&#038;rel=0&#038;showinfo=0', $data->html);
        return $data->html;
    } else return $return;
}
function sp_custom_youtube_settings($code){
	if(strpos($code, 'youtu.be') !== false || strpos($code, 'youtube.com') !== false){
		$return = preg_replace("@src=(['\"])?([^'\">\s]*)@", "src=$1$2&showinfo=0&rel=0&autohide=1", $code);
		return $return;
	}
	return $code;
}
