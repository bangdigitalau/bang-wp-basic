/**
 * Javascript for the Theme
 */
jQuery(function($) {
    /**
     * Our Theme Frontend JS
     */
    var Bang_Frontend_JS = {
        /**
         * Initialize variations actions
         */
        init: function() {
            this.theme_init();
            this.banner_adjust();
            this.chosen();
            this.check_youtube();
            this.slick_carousel();
            this.matchheight();
            this.fancybox();
            this.member_adjust();
            return this;
        },
        /***
         * Init Theme JS
         * @returns {undefined}
         */
        theme_init: function() {

        	// JS handling the Take the Next Step form
        	jQuery(document).bind('gform_post_render', function(){
        		$('.page-template-page-nextstep .showroom-appointment select').chosen({disable_search_threshold: 100,width: '100%'});
        		$('.page-template-page-nextstep .showroom-appointment .nextstep-areyou .gfield_radio input[type="radio"]:checked').each(function() {
        			var currentLabel = $(this).siblings('label').find('img').attr('src');
        			var newLabel = currentLabel.replace(/\.[^/.]+$/, "") + '-hover.png';
        			$(this).siblings('label').find('img').attr('src',newLabel);
        		});
        		$('.page-template-page-nextstep .showroom-appointment .gform_book .gfield_radio input[type="radio"][value="Yes"]').each(function() {
        			if($(this).is(':checked')) {
        				console.log($(this));
        				$('.showroom-appointment .to_show').each(function() {
        					if ($(this).hasClass('gf_left_half') || $(this).hasClass('gf_right_half')) {
        						$(this).css('display','inline-block');
        					} else {
        						$(this).css('display','list-item');
        					}
        				});
        			} else {
        				$('.showroom-appointment .to_show').each(function() {
        					$(this).css('display','none');
        				});
        			}
        		});
        	});

        	// JS handling the Take the Next Step form
        	$(".page-template-page-nextstep .gfield_radio label, .page-template-page-nextstep .gfield_checkbox label").click(function(e){
        	    e.preventDefault();
        	    $("#"+$(this).attr("for")).click().change();
        	});

        	// JS handling the Take the Next Step form
        	$(document).on('change', function() {
        		$('.page-template-page-nextstep .showroom-appointment .nextstep-areyou .gfield_radio input[type="radio"]').each(function() {
        			if($(this).is(':checked')) {
        				var currentLabel = $(this).siblings('label').find('img').attr('src');
        				if (currentLabel.indexOf("hover") < 0) {
        					var newLabel = currentLabel.replace(/\.[^/.]+$/, "") + '-hover.png';
        					$(this).siblings('label').find('img').attr('src',newLabel);
        				}
        			} else {
        				var currentLabel = $(this).siblings('label').find('img').attr('src');
        				if (currentLabel.indexOf("hover") > 0) {
        					var newLabel = currentLabel.replace("-hover", "");
        					$(this).siblings('label').find('img').attr('src',newLabel);
        				}
        			}
        		});
        		$('.page-template-page-nextstep .showroom-appointment .gform_book .gfield_radio input[type="radio"][value="Yes"]').each(function() {
        			if($(this).is(':checked')) {
        				$('.showroom-appointment .to_show').each(function() {
        					if ($(this).hasClass('gf_left_half') || $(this).hasClass('gf_right_half')) {
        						$(this).css('display','inline-block');
        					} else {
        						$(this).css('display','list-item');
        					}
        				});
        			} else {
        				$('.showroom-appointment .to_show').each(function() {
        					$(this).css('display','none');
        				});
        			}
        		});
        	});

            //Dealer search change - toggle qld postcode box
        	//Detect when the field changes
        	$('#region-filter').change(function() {

        		//Set up vars
        		var selection = $('#region-filter option:selected').val();
        		var postCodeWrap = $("#postcode-search");
        		var postCodeInput = $('#postcode-search input[name=postcode]');
        		var hideEffect = $("#postcode-search").data('hideEffect');

        		if(selection=="qld"){
        			//Show/disable the postcode
        			if(hideEffect == 'hide'){
        				$(postCodeWrap).val('').fadeIn();
        			} else {
        				$(postCodeInput).val('').prop('disabled',false);
        			}

        			//Make postcode field required
        			$(postCodeInput).prop('required',true);

        		//Else - hide box and clear field
        		} else {
        			//Hide/disable the postcode field
        			if(hideEffect == 'hide'){
        				$(postCodeWrap).fadeOut();
        			}else{
        				$(postCodeInput).prop('disabled',true);
        			}
        			//Reset the field content to blank (so we dont submit the old value)
        			$(postCodeInput).val('');
        			//Remove required attribute from postcode
        			$(postCodeInput).prop('required',false);
        		}
        	});

        	// JS to handle the Find a specialist form
        	$('#dealer-search-form #category-filter, #dealer-search-form #region-filter, #dealer-search-inline #category-filter, #dealer-search-inline #region-filter').on('change', function() {

        		var catSelected = $('#dealer-search-form #category-filter, #dealer-search-inline #category-filter').val();
        		var regionSelected = $('#dealer-search-form #region-filter, #dealer-search-inline #region-filter').val();

        		console.log('changed');
        		console.log(catSelected);
        		console.log(regionSelected);
        		console.log('-----');
        		// Postcode search only for:
        		// All QLD
        		// WA & Mylights
        		if((catSelected != 'my-lights' && regionSelected == 'wa') || regionSelected == 'qld') {
        			$('.page-template-page-specialist-search #postcode-search').show();
        			$('#postcode-search .form-control').prop('disabled',false);
        			$('#postcode-search input').prop('required',true);
        		} else {
        			$('.page-template-page-specialist-search #postcode-search').hide();
        			$('#postcode-search .form-control').prop('disabled',true);
        			$('#postcode-search input').val('').prop('required',false);
        		}
        	});

            // Main Menu Fixed on Scroll
            $(window).on('scroll', function(e) {

                fixed_navigation(e);

            });

            function fixed_navigation(e){
                top_bar_offset = $('.top-bar').height();
                header_offset = $('.header').height();
                header_offset_mobile = $('.header-mobile').height();

                if (($(window).width() > 768 && $(window).scrollTop() > top_bar_offset)) {
                    $('.header').addClass("fixed");
                    // Smooth transition for fixed-menu
                    $('body').css('padding-top',header_offset+'px');
                }else if($(window).width() < 768 ){
                    $('.header').addClass("fixed");
                    // Smooth transition for fixed-menu
                    $('body').css('padding-top',header_offset_mobile+'px');
                } else {
                    $('.header').removeClass("fixed");
                    $('body').css('padding-top','0');
                }
            }
            fixed_navigation();

            // Mobile Main menu clicks
            $('.mobile-menu i').on('click', function(e) {
                e.preventDefault();
                var menustate = $('.mobile-main-menu').css('opacity');
                var headerheight = $('.header-mobile').outerHeight();
                var additionalheight = 46;
                var totalheight = headerheight + additionalheight;
                var windowheight = $(window).height();
                var menuheight = windowheight - headerheight;
                console.log(menuheight);

                if (menustate == '0') {
                    $('.mobile-main-menu').css('opacity', '1').css('height', menuheight).css('top', headerheight+'px');
                    $(this).removeClass('fa-bars').addClass('fa-times');
                    $('body').css('overflow', 'hidden');
                } else {
                    $('.mobile-main-menu').css('opacity', '0').css('height', '0');
                    $(this).removeClass('fa-times').addClass('fa-bars');
                    $('body').css('overflow', '');
                }
            });

            // Mobile Main Menu Submenu add arrows
            $('.mobile-main-menu .menu-item-has-children').each(function() {
                $(this).append('<i class="fas fa-angle-down"></i>');
            });

            // Mobile Main Menu Submenu click
            $('.mobile-main-menu .menu-item-has-children .fas, .mobile-main-menu .menu-item-has-children a[href=#]').on('click', function(e) {
                var childstate = $(this).siblings('.sub-menu').css('display');
                if(childstate == 'none') {
                    e.preventDefault();
                    $('.mobile-main-menu .sub-menu').each(function() {
                        $(this).css('display', 'none');
                    });
                    $(this).siblings('.sub-menu').css('display','block');
                } else {
                    e.preventDefault();
                    $(this).siblings('.sub-menu').css('display','none');
                }
            });

            // ignore link click if the href is #
            $('a[href=#]').on('click', function(e) {
                e.preventDefault();
            });

        },
        chosen: function() {
        	// chosen
        	$('.dealer-search select.form-control').chosen({width: '100%', disable_search_threshold: 1});
        	$('.page-template-page-nextstep .showroom-appointment select').chosen({disable_search_threshold: 100, width: '100%'});
        },
        banner_adjust: function() {
            if ($('.banner-item-content-img').length > 0) {
                var bannerHeight = $('.banner-item.withimage').outerHeight();
                var bannerItemHeight = $('.banner-item.withimage .banner-item-content').outerHeight();
                var marginBottom = bannerItemHeight - bannerHeight + 20;
                $('.banner-item.withimage').css('margin-bottom', marginBottom);
            }
        },
        check_youtube: function() {
            if ($('.two-column-item-content-txt iframe[src*="youtube"]').length > 0) {
                var iframeparent = $('.two-column-item-content-txt iframe[src*="youtube"]').parents('.two-column-item-content-txt');
                $(iframeparent).addClass('yt-embed');
            }
        },
        slick_carousel: function() {
            $('.slick-carousel').slick({
                arrows: false,
                adaptiveHeight: true
            });
            $('.team_member_slick .nav-prev').on('click', function(e) {
                e.preventDefault();
                $('.team_member_slick').slick('slickPrev');
            });
            $('.team_member_slick .nav-next').on('click', function(e) {
                e.preventDefault();
                $('.team_member_slick').slick('slickNext');
            });
            $('.testimonials_slick .nav-prev').on('click', function(e) {
                e.preventDefault();
                $('.testimonials_slick').slick('slickPrev');
            });
            $('.testimonials_slick .nav-next').on('click', function(e) {
                e.preventDefault();
                $('.testimonials_slick').slick('slickNext');
            });

        },
        member_adjust: function() {
            var sectionwidth = $('.team-member-section').outerWidth();
            if (sectionwidth < 975) {
                $('.team-member-section-item').each(function() {
                    $(this).css('height', 'auto');
                });
            } else {
                $('.team-member-section-item').each(function() {
                    var panelheight = $(this).data('panelheight');
                    $(this).css('height', panelheight);
                });
            }
        },
        matchheight: function() {
            $('.js-match-height').matchHeight();
        },
        fancybox: function() {
            $('a[href*="youtube"][target!="_blank"]').fancybox({
                type:'iframe',
                padding: 0,
                tpl: {
                    closeBtn: '<a href="javascript:;" class="fancybox-item fancybox-new-close"><i class="fas fa-times"></i></a>'
                }
            });
        }
    };
    $( window ).on( 'load', function() {
        Bang_Frontend_JS.banner_adjust();
        Bang_Frontend_JS.member_adjust();
    });
    $( window ).resize(function() {
        Bang_Frontend_JS.banner_adjust();
        Bang_Frontend_JS.matchheight();
        Bang_Frontend_JS.chosen();
        Bang_Frontend_JS.member_adjust();
    });
    Bang_Frontend_JS.init();
});
