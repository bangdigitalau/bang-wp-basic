<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
$logo = get_field('logo', 'option');
$logo_url = wp_get_attachment_image_src($logo, 'full')[0];
$find_specialist_page = get_field('find_specialist_page', 'option');
$contact_page = get_field('contact_page', 'option');
$facebook = get_field('facebook', 'option');
$youtube = get_field('youtube', 'option');
$linkedin = get_field('linkedin', 'option');
$twitter = get_field('twitter', 'option');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="main-wrap">

    <div class="top-bar d-none d-md-block">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php
                    /**
                     * Top Menu
                     */
                    wp_nav_menu(array(
                        'theme_location' => 'top-menu',
                        'menu_class' => 'top-menu',
                        'container' => ''
                    ));
                    ?>
                    <?php if($facebook || $youtube || $twitter || $linkedin) : ?>
                    <div class="sm-link">
                        <?php if($facebook) : ?>
                            <span class="fb"><a href="<?php echo $facebook; ?>" target="_blank"><i class="fab fa-facebook"></i></a></span>
                        <?php endif; ?>
                        <?php if($youtube) : ?>
                            <span class="yt"><a href="<?php echo $youtube; ?>" target="_blank"><i class="fab fa-youtube"></i></a></span>
                        <?php endif; ?>
                        <?php if($twitter) : ?>
                            <span class="tw"><a href="<?php echo $twitter; ?>" target="_blank"><i class="fab fa-twitter"></i></a></span>
                        <?php endif; ?>
                        <?php if($linkedin) : ?>
                            <span class="li"><a href="<?php echo $linkedin; ?>" target="_blank"><i class="fab fa-linkedin"></i></a></span>
                        <?php endif; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="header d-none d-md-block">
        <div class="container">
            <div class="row">
                <div class="col-md-2 header-logo">
                    <a href="<?php echo get_home_url(); ?>" title="<?php echo get_bloginfo('name')?>"><img src="<?php echo $logo_url; ?>"></a>
                </div>
                <div class="col-md-8">
                    <?php
                    /**
                     * Main Menu
                     */
                    wp_nav_menu(array(
                        'theme_location' => 'main-menu',
                        'menu_class' => 'main-menu',
                        'container' => ''
                    ));
                    ?>
                </div>
                <div class="col-md-2 header-cta">
                    <a href="<?php echo $contact_page; ?>" class="btn darkblue-bg white-txt">Get in touch</a>
                </div>
            </div>
        </div>
        <div class="hr"></div>
    </div>

    <div class="header header-mobile d-block d-md-none">
        <div class="container">
            <div class="row">
                <div class="col-4 header-logo">
                    <a href="<?php echo get_home_url(); ?>" title="<?php echo get_bloginfo('name')?>"><img src="<?php echo $logo_url; ?>"></a>
                </div>
                <div class="col-6">
                </div>
                <div class="col-2 mobile-menu">
                    <i class="fas fa-bars"></i>
                    <?php
                    /**
                     * Main Menu
                     */
                    wp_nav_menu(array(
                        'theme_location' => 'main-menu',
                        'menu_class' => 'mobile-main-menu',
                        'container' => ''
                    ));
                    ?>
                </div>
            </div>
        </div>
        <div class="hr"></div>
    </div>
