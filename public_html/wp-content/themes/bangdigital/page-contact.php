<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 * Template Name: Contact Us Page
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header();

$contact_short_description = get_field('contact_short_description');
$contact_cta = get_field('contact_cta');
?>

<div id="primary" class="content-area contact-us main-content-area">
	<main id="main" class="site-main" role="main">

		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="row">
						<div class="col-12 col-md-<?php if($contact_cta) : ?>8<?php else: ?>12<?php endif; ?>">
							<?php get_template_part( 'template-parts/heading', 'page' ); ?>
							<?php if($contact_short_description) : ?>
								<?php echo $contact_short_description; ?>
							<?php endif; ?>
						</div>
						<?php if($contact_cta) : ?>
						<div class="col-12 col-md-4">
							<div class="contact-us-cta">
								<?php echo $contact_cta; ?>
							</div>
						</div>
						<?php endif; ?>
					</div>
					<?php if( have_rows('main_location') ): ?>
						<?php while ( have_rows('main_location') ) : the_row(); ?>
							<?php
								$main_location_anchor = get_sub_field('main_location_anchor');
								$main_location_name = get_sub_field('main_location_name');
								$main_location_address = get_sub_field('main_location_address');
								$main_location_map = get_sub_field('main_location_map');
								$main_location_directions = get_sub_field('main_location_directions');
								$main_location_hours = get_sub_field('main_location_hours');
								$main_location_phone = get_sub_field('main_location_phone');
								$main_location_fax = get_sub_field('main_location_fax');
								$main_location_installer_support = get_sub_field('main_location_installer_support');
								$main_location_email = get_sub_field('main_location_email');
							?>
						<div class="row">
							<div class="col-md-5">
								<div class="main-location-item"<?php if($main_location_anchor) : ?> id="<?php echo $main_location_anchor; ?>"<?php endif; ?>>
									<?php if($main_location_name) : ?>
									<h2><?php echo $main_location_name; ?></h2>
									<?php endif; ?>
									<?php if($main_location_address) : ?>
									<div class="main-location-item-address">
										<?php echo $main_location_address; ?>
									</div>
									<?php endif; ?>
									<?php if($main_location_directions) : ?>
									<div class="main-location-item-button">
										<a href="<?php echo $main_location_directions; ?>" class="btn darkblue-bg white-txt">Get Directions</a>
									</div>
									<?php endif; ?>
									<?php if($main_location_hours) : ?>
									<div class="main-location-item-hours">
										<span>Hours:</span>
										<?php echo $main_location_hours; ?>
									</div>
									<?php endif; ?>
									<?php if($main_location_phone) : ?>
									<div class="main-location-item-phone">
										<span>Phone:</span>
										<a href="tel:<?php echo $main_location_phone; ?>"><?php echo $main_location_phone; ?></a>
									</div>
									<?php endif; ?>
									<?php if($main_location_fax) : ?>
									<div class="main-location-item-fax">
										<span>Fax:</span>
										<?php echo $main_location_fax; ?>
									</div>
									<?php endif; ?>
									<?php if($main_location_installer_support) : ?>
									<div class="main-location-item-support">
										<span>Installer Support:</span>
										<a href="tel:<?php echo $main_location_installer_support; ?>"><?php echo $main_location_installer_support; ?></a>
									</div>
									<?php endif; ?>
									<?php if($main_location_email) : ?>
									<div class="main-location-item-email">
										<span>Email:</span>
										<a href="mailto:<?php echo $main_location_email; ?>"><?php echo $main_location_email; ?></a>
									</div>
									<?php endif; ?>
								</div>
							</div>
							<div class="col-md-7">
								<div class="main-location-item">
									<?php if($main_location_map) : ?>
									<div class="main-location-item-map">
										<?php echo $main_location_map; ?>
									</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<hr>
							</div>
						</div>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php if( have_rows('additional_location') ): ?>
						<div class="row">
						<?php while ( have_rows('additional_location') ) : the_row(); ?>
							<?php
								$additional_location_country = get_sub_field('additional_location_country');
								$additional_location_name = get_sub_field('additional_location_name');
								$additional_location_address = get_sub_field('additional_location_address');
								$additional_location_contact_details = get_sub_field('additional_location_contact_details');
							?>
							<div class="col-md-3">
								<div class="additional-location-item">
									<h2<?php if(!$additional_location_country) : ?> class="d-none d-md-block"<?php endif; ?>><?php if($additional_location_country) : ?><?php echo $additional_location_country; ?><?php else : ?>&nbsp;<?php endif; ?></h2>
									<h3><?php echo $additional_location_name; ?></h3>
									<?php if($additional_location_address) : ?>
									<div class="additional-location-item-address">
										<?php echo $additional_location_address; ?>
									</div>
									<?php endif; ?>
									<?php if($additional_location_contact_details) : ?>
									<div class="additional-location-item-contact">
										<?php echo $additional_location_contact_details; ?>
									</div>
									<?php endif; ?>
								</div>
							</div>
						<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

	</main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
