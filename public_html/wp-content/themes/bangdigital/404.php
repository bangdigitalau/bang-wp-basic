<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<div id="primary" class="content-area main-content-area">
	<main id="main" class="site-main" role="main">

		<div class="container">
			<div class="row">
				<div class="col-12">
					<section class="error-404 not-found">
						<?php get_template_part( 'template-parts/heading', 'page' ); ?>
						<div class="page-content">
							<p><?php _e( 'Page not found. Try using the search form below, or <a href="/">return to the home page</a>.', 'twentysixteen' ); ?></p>

							<?php get_search_form(); ?>
						</div><!-- .page-content -->
					</section><!-- .error-404 -->
				</div>
			</div>
		</div>

	</main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
