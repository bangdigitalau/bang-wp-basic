<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php
$homebanner_bg_type = get_field('homebanner_bg_type');
if ($homebanner_bg_type == 'image') {
	$homebanner_bg_image = get_field('homebanner_bg_image');
	$homebanner_bg_image_url = wp_get_attachment_image_src($homebanner_bg_image,'full')[0];
} elseif ($homebanner_bg_type == 'color') {
	$homebanner_bg_color = get_field('homebanner_bg_color');
}
$homebanner_text_color = get_field('homebanner_text_color');
$homebanner_heading = get_field('homebanner_heading');
$homebanner_content = get_field('homebanner_content');
$homebanner_bg_image_alignment = false ?: '' ?: 'align-'.get_field('homebanner_bg_image_alignment');
$homebanner_content_aligment = false ?: '' ?: 'align-'.get_field('homebanner_content_aligment');
$homebanner_content_text_shadow_boost = false ?: '' ?: 'shadow-boost';

?>
<?php if ($homebanner_bg_type) : ?>
<?php if ($homebanner_bg_type != 'none') : ?>
<div class="banner<?php if ($homebanner_heading || $homebanner_content) : ?> banner-overlay<?php endif; ?>">
	<div class="<?php echo $homebanner_bg_image_alignment ?> banner-item<?php if($homebanner_bg_color) : ?> <?php echo $homebanner_bg_color ?>-bg<?php endif; ?><?php if($homebanner_text_color) : ?> <?php echo $homebanner_text_color ?>-txt<?php endif; ?>"<?php if($homebanner_bg_image_url) : ?> style="background-image: url(<?php echo $homebanner_bg_image_url; ?>);"<?php endif; ?>>
		<div class="container">
			<div class="row">
				<div class="col-12 <?php echo $homebanner_content_aligment ?>">
					<div class="banner-item-content">
						<h1 class="<?php echo $homebanner_content_text_shadow_boost ?>"><?php echo $homebanner_heading; ?></h1>
						<div class="banner-item-content-txt">
							<?php echo $homebanner_content; ?>
						</div>
						<?php if( have_rows('homebanner_icons') ): ?>
						<div class="banner-item-icons">
							<?php while ( have_rows('homebanner_icons') ) : the_row(); ?>
								<?php
									$homebanner_icon_image = get_sub_field('homebanner_icon_image');
									$homebanner_icon_image_url = wp_get_attachment_image_src($homebanner_icon_image,'full')[0];
									$homebanner_icon_url = get_sub_field('homebanner_icon_url');
									$homebanner_open_new_tab = get_sub_field('homebanner_open_new_tab');
								?>
								<div class="banner-item-icons-items">
									<?php if($homebanner_icon_url) : ?><a href="<?php echo $homebanner_icon_url; ?>"<?php if($homebanner_open_new_tab) : ?> target="_blank"<?php endif; ?>><?php endif; ?>
									<img src="<?php echo $homebanner_icon_image_url; ?>">
									<?php if($homebanner_icon_url) : ?></a><?php endif; ?>
								</div>
							<?php endwhile; ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php endif; ?>

<?php
$mainbanner_background_type = get_field('mainbanner_background_type');
if ($mainbanner_background_type == 'image') {
	$mainbanner_bg_image = get_field('mainbanner_bg_image');
	$mainbanner_bg_image_url = wp_get_attachment_image_src($mainbanner_bg_image,'full')[0];
} elseif ($mainbanner_background_type == 'color') {
	$mainbanner_bg_color = get_field('mainbanner_bg_color');
}
$mainbanner_text_color = get_field('mainbanner_text_color');
$mainbanner_heading = get_field('mainbanner_heading');
$mainbanner_content = get_field('mainbanner_content');
$mainbanner_show_image = get_field('mainbanner_show_image');
if ($mainbanner_show_image) {
	$mainbanner_product_image = get_field('mainbanner_product_image');
	$mainbanner_product_image_url = wp_get_attachment_image_src($mainbanner_product_image,'full')[0];
	$mainbanner_product_image_height = wp_get_attachment_image_src($mainbanner_product_image,'full')[2];
	$mainbanner_margin_bottom = ($mainbanner_product_image_height / 2);
}
?>
<?php if ($mainbanner_background_type) : ?>
<?php if ($mainbanner_background_type != 'none') : ?>
<div class="banner<?php if ($mainbanner_heading || $mainbanner_content) : ?> banner-overlay<?php endif; ?><?php if($mainbanner_show_image) : ?> banner-withimage<?php endif; ?>">
	<div class="banner-item<?php if($mainbanner_show_image) : ?> withimage<?php endif; ?><?php if($mainbanner_bg_color) : ?> <?php echo $mainbanner_bg_color ?>-bg<?php endif; ?><?php if($mainbanner_text_color) : ?> <?php echo $mainbanner_text_color ?>-txt<?php endif; ?>" style="<?php if($mainbanner_bg_image_url) : ?>background-image: url(<?php echo $mainbanner_bg_image_url; ?>);<?php endif; ?>">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="banner-item-content">
						<h1><?php echo $mainbanner_heading; ?></h1>
						<div class="banner-item-content-txt">
							<?php echo $mainbanner_content; ?>
						</div>
						<?php if ($mainbanner_show_image) : ?>
							<div class="banner-item-content-img">
								<img src="<?php echo $mainbanner_product_image_url; ?>">
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php endif; ?>

<div class="flexible-content">
<?php if( have_rows('flexible_content') ): ?>
	<?php $count = 1; ?>
	<?php while ( have_rows('flexible_content') ) : the_row(); ?>

		<?php if( get_row_layout() == 'one_column' ): ?>
			<?php
				$one_column_bg_color = get_sub_field('one_column_bg_color');
				$one_column_text_color = get_sub_field('one_column_text_color');
				$one_column_heading = get_sub_field('one_column_heading');
				$one_column_content = get_sub_field('one_column_content');
				$one_column_show_button = get_sub_field('one_column_show_button');
				if ($one_column_show_button) {
					$one_column_button_text = get_sub_field('one_column_button_text');
					$one_column_button_link = get_sub_field('one_column_button_link');
					$one_column_open_new_tab = get_sub_field('one_column_open_new_tab');
					$one_column_button_bg_color = get_sub_field('one_column_button_bg_color');
					$one_column_button_text_color = get_sub_field('one_column_button_text_color');
				}
			?>
			<div class="one-column layout<?php echo $count; ?><?php if($one_column_bg_color) : ?> <?php echo $one_column_bg_color ?>-bg<?php endif; ?><?php if($one_column_text_color) : ?> <?php echo $one_column_text_color ?>-txt<?php endif; ?>">
				<div class="one-column-item">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<?php if ($one_column_heading) : ?><h2><?php echo $one_column_heading; ?></h2><?php endif; ?>
								<div class="one-column-item-content-txt">
									<?php echo $one_column_content; ?>
								</div>
								<?php if ($one_column_show_button) : ?>
								<div class="one-column-item-content-btn">
									<a class="btn <?php echo $one_column_button_bg_color ?>-bg <?php echo $one_column_button_text_color; ?>-txt" href="<?php echo $one_column_button_link; ?>"<?php if($one_column_open_new_tab) : ?> target="_blank"<?php endif; ?>><?php echo $one_column_button_text; ?></a>
								</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php elseif ( get_row_layout() == 'two_column' ) : ?>
			<?php
				$two_column_layout = get_sub_field('two_column_layout');
				$two_column_image_type = get_sub_field('two_column_image_type');
				if ($two_column_image_type == 'full') {
					$two_column_image_alignment = get_sub_field('two_column_image_alignment');
				}
				$two_column_bg_color = get_sub_field('two_column_bg_color');
				$two_column_text_color = get_sub_field('two_column_text_color');
				$two_column_image = get_sub_field('two_column_image');
				$two_column_image_url = wp_get_attachment_image_src($two_column_image,'full')[0];
				$two_column_heading = get_sub_field('two_column_heading');
				$two_column_content = get_sub_field('two_column_content');
				$two_column_show_button = get_sub_field('two_column_show_button');
				if ($two_column_show_button) {
					$two_column_button_text = get_sub_field('two_column_button_text');
					$two_column_button_link = get_sub_field('two_column_button_link');
					$two_column_open_new_tab = get_sub_field('two_column_open_new_tab');
					$two_column_button_bg_color = get_sub_field('two_column_button_bg_color');
					$two_column_button_text_color = get_sub_field('two_column_button_text_color');
				}
			?>
			<div class="two-column layout<?php echo $count; ?><?php if($two_column_bg_color) : ?> <?php echo $two_column_bg_color ?>-bg<?php endif; ?><?php if($two_column_text_color) : ?> <?php echo $two_column_text_color ?>-txt<?php endif; ?>">
				<?php if($two_column_layout == 'ltri') : ?>
				<div class="two-column-item ltri">
					<?php if($two_column_image_type == 'full') : ?>
					<div class="container-fluid full-size-image d-none d-md-block">
						<div class="row">
							<div class="col-md-6">
							</div>
							<div class="col-md-6 two-column-item-img<?php if($two_column_image_alignment == 'top') : ?> img-align-top<?php elseif ($two_column_image_alignment == 'bottom') : ?> img-align-bottom<?php endif; ?>" style="background-image: url(<?php echo $two_column_image_url; ?>);" data-mh="two-column-item<?php echo $count; ?>">
							</div>
						</div>
					</div>
					<?php endif; ?>
					<div class="container<?php if($two_column_image_type == 'inset') : ?> inset<?php endif; ?>">
						<div class="row">
							<div class="col-md-6 two-column-item-content" data-mh="two-column-item<?php echo $count; ?>">
								<?php if($two_column_heading) : ?><h2><?php echo $two_column_heading; ?></h2><?php endif; ?>
								<div class="two-column-item-content-txt">
									<?php echo $two_column_content; ?>
								</div>
								<?php if ($two_column_show_button) : ?>
								<div class="two-column-item-content-btn">
									<a class="btn <?php echo $two_column_button_bg_color ?>-bg <?php echo $two_column_button_text_color; ?>-txt" href="<?php echo $two_column_button_link; ?>"<?php if($two_column_open_new_tab) : ?> target="_blank"<?php endif; ?>><?php echo $two_column_button_text; ?></a>
								</div>
								<?php endif; ?>
							</div>
							<div class="col-md-6 two-column-item-inset-img" data-mh="two-column-item<?php echo $count; ?>">
								<?php if($two_column_image_type == 'inset') : ?>
									<img src="<?php echo $two_column_image_url; ?>">
								<?php else : ?>
									<img class="d-block d-md-none" src="<?php echo $two_column_image_url; ?>">
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				<?php elseif ($two_column_layout == 'lirt') : ?>
				<div class="two-column-item ltri">
					<?php if($two_column_image_type == 'full') : ?>
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6 two-column-item-img<?php if($two_column_image_alignment == 'top') : ?> img-align-top<?php elseif ($two_column_image_alignment == 'bottom') : ?> img-align-bottom<?php endif; ?>" style="background-image: url(<?php echo $two_column_image_url; ?>);" data-mh="two-column-item<?php echo $count; ?>">
							</div>
							<div class="col-md-6">
							</div>
						</div>
					</div>
					<?php endif; ?>
					<div class="container<?php if($two_column_image_type == 'inset') : ?> inset<?php endif; ?>">
						<div class="row">
							<div class="col-md-6 two-column-item-inset-img">
								<?php if($two_column_image_type == 'inset') : ?>
									<img src="<?php echo $two_column_image_url; ?>">
								<?php endif; ?>
							</div>
							<div class="col-md-6 two-column-item-content" data-mh="two-column-item<?php echo $count; ?>">
								<?php if($two_column_heading) : ?><h2><?php echo $two_column_heading; ?></h2><?php endif; ?>
								<div class="two-column-item-content-txt">
									<?php echo $two_column_content; ?>
								</div>
								<?php if ($two_column_show_button) : ?>
								<div class="two-column-item-content-btn">
									<a class="btn <?php echo $two_column_button_bg_color ?>-bg <?php echo $two_column_button_text_color; ?>-txt" href="<?php echo $two_column_button_link; ?>"<?php if($two_column_open_new_tab) : ?> target="_blank"<?php endif; ?>><?php echo $two_column_button_text; ?></a>
								</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				<?php elseif ($two_column_layout == 'btxt') : ?>
				<?php
					$two_column_heading_second = get_sub_field('two_column_heading_second');
					$two_column_content_second = get_sub_field('two_column_content_second');
					$two_column_show_button_second = get_sub_field('two_column_show_button_second');
					if($two_column_show_button_second) {
						$two_column_button_text_second = get_sub_field('two_column_button_text_second');
						$two_column_button_link_second = get_sub_field('two_column_button_link_second');
						$two_column_open_new_tab_second = get_sub_field('two_column_open_new_tab_second');
						$two_column_button_bg_color_second = get_sub_field('two_column_button_bg_color_second');
						$two_column_button_text_color_second = get_sub_field('two_column_button_text_color_second');
					}
				?>
				<div class="two-column-item btxt">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-6 two-column-item-content" data-mh="two-column-item<?php echo $count; ?>">
								<?php if($two_column_heading) : ?><h2><?php echo $two_column_heading; ?></h2><?php endif; ?>
								<div class="two-column-item-content-txt">
									<?php echo $two_column_content; ?>
								</div>
								<?php if ($two_column_show_button) : ?>
								<div class="two-column-item-content-btn">
									<a class="btn <?php echo $two_column_button_bg_color ?>-bg <?php echo $two_column_button_text_color; ?>-txt" href="<?php echo $two_column_button_link; ?>"<?php if($two_column_open_new_tab) : ?> target="_blank"<?php endif; ?>><?php echo $two_column_button_text; ?></a>
								</div>
								<?php endif; ?>
							</div>
							<div class="col-12 col-md-6 two-column-item-content" data-mh="two-column-item<?php echo $count; ?>">
								<?php if($two_column_heading) : ?><h2><?php echo $two_column_heading_second; ?></h2><?php endif; ?>
								<div class="two-column-item-content-txt">
									<?php echo $two_column_content_second; ?>
								</div>
								<?php if ($two_column_show_button_second) : ?>
								<div class="two-column-item-content-btn">
									<a class="btn <?php echo $two_column_button_bg_color_second ?>-bg <?php echo $two_column_button_text_color_second; ?>-txt" href="<?php echo $two_column_button_link_second; ?>"<?php if($two_column_open_new_tab_second) : ?> target="_blank"<?php endif; ?>><?php echo $two_column_button_text_second; ?></a>
								</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php elseif ($two_column_layout == 'ltrd') : ?>
				<?php
					$two_column_heading_download = get_sub_field('two_column_heading_download');
					$two_column_heading_icon = get_sub_field('two_column_heading_icon');
					$two_column_heading_color = get_sub_field('two_column_heading_color');
					$two_column_heading_text_color = get_sub_field('two_column_heading_text_color');
					$myair_icon = get_field('myair_icon', 'option');
					$myair_icon_url = wp_get_attachment_image_src($myair_icon,'full')[0];
					$mylights_icon = get_field('mylights_icon', 'option');
					$mylights_icon_url = wp_get_attachment_image_src($mylights_icon,'full')[0];
					$myplace_icon = get_field('myplace_icon', 'option');
					$myplace_icon_url = wp_get_attachment_image_src($myplace_icon,'full')[0];
					$myteam_icon = get_field('myteam_icon', 'option');
					$myteam_icon_url = wp_get_attachment_image_src($myteam_icon,'full')[0];
				?>
				<div class="two-column-item ltrd">
					<div class="container">
						<div class="row">
							<div class="col-md-7 two-column-item-content" data-mh="two-column-item<?php echo $count; ?>">
								<?php if($two_column_heading) : ?><h2><?php echo $two_column_heading; ?></h2><?php endif; ?>
								<div class="two-column-item-content-txt">
									<?php echo $two_column_content; ?>
								</div>
								<?php if ($two_column_show_button) : ?>
								<div class="two-column-item-content-btn">
									<a class="btn <?php echo $two_column_button_bg_color ?>-bg <?php echo $two_column_button_text_color; ?>-txt" href="<?php echo $two_column_button_link; ?>"<?php if($two_column_open_new_tab) : ?> target="_blank"<?php endif; ?>><?php echo $two_column_button_text; ?></a>
								</div>
								<?php endif; ?>
							</div>
							<div class="col-md-5 two-column-item-content two-column-download" data-mh="two-column-item<?php echo $count; ?>">
								<div class="row">
									<div class="col-12">
										<div class="two-column-download-header <?php echo $two_column_heading_color ?>-bg <?php echo $two_column_heading_text_color; ?>-txt">
											<span><?php echo $two_column_heading_download; ?></span>
											<?php if ($two_column_heading_icon != 'none') : ?>
												<div class="two-column-download-img">
												<?php
												if ($two_column_heading_icon == 'myair') {
													echo "<img src='".$myair_icon_url."'>";
												} elseif ($two_column_heading_icon == 'mylights') {
													echo "<img src='".$mylights_icon_url."'>";
												} elseif ($two_column_heading_icon == 'myteam') {
													echo "<img src='".$myteam_icon_url."'>";
												} elseif ($two_column_heading_icon == 'myplace') {
													echo "<img src='".$myplace_icon_url."'>";
												}
												?>
												</div>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<?php if( have_rows('two_column_download') ): ?>
									<?php while ( have_rows('two_column_download') ) : the_row(); ?>
									<?php
										$two_column_file_name = get_sub_field('two_column_file_name');
										$two_column_file = get_sub_field('two_column_file');
										$two_column_file_url = wp_get_attachment_url($two_column_file);
									?>
									<div class="row">
										<div class="col-12">
											<a class="two-column-download-link darkgrey-bg white-txt" href="<?php echo $two_column_file_url; ?>" target="_blank"><?php echo $two_column_file_name; ?></a>
										</div>
									</div>
									<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		<?php elseif ( get_row_layout() == 'three_column' ) : ?>
			<?php
				$three_column_bg_color = get_sub_field('three_column_bg_color');
				$three_column_text_color = get_sub_field('three_column_text_color');
				$three_column_heading = get_sub_field('three_column_heading');
				$three_column_section_top_content = get_sub_field('three_column_section_top_content');
				$three_column_section_bottom_content = get_sub_field('three_column_section_bottom_content');
				$three_column_show_button_end = get_sub_field('three_column_show_button_end');
				if ($three_column_show_button_end) {
					$three_column_button_text = get_sub_field('three_column_button_text');
					$three_column_button_link = get_sub_field('three_column_button_link');
					$three_column_open_new_tab = get_sub_field('three_column_open_new_tab');
					$three_column_button_bg_color = get_sub_field('three_column_button_bg_color');
					$three_column_button_text_color = get_sub_field('three_column_button_text_color');
				}
				$three_column_show_border = get_sub_field('three_column_show_border');
				if ($three_column_show_border) {
					$three_column_border_color = get_sub_field('three_column_border_color');
				}
				// First Column
				$three_column_first_column_heading = get_sub_field('three_column_first_column_heading');
				$three_column_first_column_content = get_sub_field('three_column_first_column_content');
				$three_column_first_column_image = get_sub_field('three_column_first_column_image');
				$three_column_first_column_image_url = wp_get_attachment_image_src($three_column_first_column_image,'full')[0];
				$three_column_first_column_show_button = get_sub_field('three_column_first_column_show_button');
				if ($three_column_first_column_show_button) {
					$three_column_first_column_button_text = get_sub_field('three_column_first_column_button_text');
					$three_column_first_column_button_link = get_sub_field('three_column_first_column_button_link');
					$three_column_first_column_open_new_tab = get_sub_field('three_column_first_column_open_new_tab');
					$three_column_first_column_button_bg_color = get_sub_field('three_column_first_column_button_bg_color');
					$three_column_first_column_button_text_color = get_sub_field('three_column_first_column_button_text_color');
				}
				// Second Column
				$three_column_second_column_heading = get_sub_field('three_column_second_column_heading');
				$three_column_second_column_content = get_sub_field('three_column_second_column_content');
				$three_column_second_column_image = get_sub_field('three_column_second_column_image');
				$three_column_second_column_image_url = wp_get_attachment_image_src($three_column_second_column_image,'full')[0];
				$three_column_second_column_show_button = get_sub_field('three_column_second_column_show_button');
				if ($three_column_second_column_show_button) {
					$three_column_second_column_button_text = get_sub_field('three_column_second_column_button_text');
					$three_column_second_column_button_link = get_sub_field('three_column_second_column_button_link');
					$three_column_second_column_open_new_tab = get_sub_field('three_column_second_column_open_new_tab');
					$three_column_second_column_button_bg_color = get_sub_field('three_column_second_column_button_bg_color');
					$three_column_second_column_button_text_color = get_sub_field('three_column_second_column_button_text_color');
				}
				// Third Column
				$three_column_third_column_heading = get_sub_field('three_column_third_column_heading');
				$three_column_third_column_content = get_sub_field('three_column_third_column_content');
				$three_column_third_column_image = get_sub_field('three_column_third_column_image');
				$three_column_third_column_image_url = wp_get_attachment_image_src($three_column_third_column_image,'full')[0];
				$three_column_third_column_show_button = get_sub_field('three_column_third_column_show_button');
				if ($three_column_third_column_show_button) {
					$three_column_third_column_button_text = get_sub_field('three_column_third_column_button_text');
					$three_column_third_column_button_link = get_sub_field('three_column_third_column_button_link');
					$three_column_third_column_open_new_tab = get_sub_field('three_column_third_column_open_new_tab');
					$three_column_third_column_button_bg_color = get_sub_field('three_column_third_column_button_bg_color');
					$three_column_third_column_button_text_color = get_sub_field('three_column_third_column_button_text_color');
				}
			?>
			<div class="three-column layout<?php echo $count; ?><?php if($three_column_bg_color) : ?> <?php echo $three_column_bg_color ?>-bg<?php endif; ?><?php if($three_column_text_color) : ?> <?php echo $three_column_text_color ?>-txt<?php endif; ?>">
				<div class="three-column-item">
					<div class="container">
						<?php if ($three_column_heading || $three_column_section_top_content) : ?>
						<div class="row">
							<div class="col-12 three-column-item-content-top">
								<h2><?php echo $three_column_heading; ?></h2>
								<div class="three-column-item-content-top-txt">
									<?php echo $three_column_section_top_content; ?>
								</div>
							</div>
						</div>
						<?php endif; ?>
						<div class="row">
							<div class="col-12 col-md-4 three-column-item-content<?php if (!($three_column_heading || $three_column_section_top_content)) : ?> no-margin-top<?php endif; ?>">
								<?php if ($three_column_first_column_image_url) : ?><img src="<?php echo $three_column_first_column_image_url ?>"><?php endif; ?>
								<h4><?php echo $three_column_first_column_heading; ?></h4>
								<div class="three-column-item-content-txt" data-mh="three-column-item-content-txt">
									<?php echo $three_column_first_column_content; ?>
								</div>
								<?php if ($three_column_first_column_show_button) : ?>
								<div class="three-column-item-content-bottom-btn">
									<a href="<?php echo $three_column_first_column_button_link; ?>" class="btn <?php echo $three_column_first_column_button_bg_color ?>-bg <?php echo $three_column_first_column_button_text_color; ?>-txt"<?php if($three_column_first_column_open_new_tab) : ?> target="_blank"<?php endif; ?>><?php echo $three_column_first_column_button_text; ?></a>
								</div>
								<?php endif; ?>
							</div>
							<div class="col-12 col-md-4 three-column-item-content<?php if (!($three_column_heading || $three_column_section_top_content)) : ?> no-margin-top<?php endif; ?><?php if(!$three_column_show_border) : ?> no-border<?php else : ?> border-<?php echo $three_column_border_color ?><?php endif; ?>">
								<?php if ($three_column_second_column_image_url) : ?><img src="<?php echo $three_column_second_column_image_url ?>"><?php endif; ?>
								<h4><?php echo $three_column_second_column_heading; ?></h4>
								<div class="three-column-item-content-txt" data-mh="three-column-item-content-txt">
									<?php echo $three_column_second_column_content; ?>
								</div>
								<?php if ($three_column_second_column_show_button) : ?>
								<div class="three-column-item-content-bottom-btn">
									<a href="<?php echo $three_column_second_column_button_link; ?>" class="btn <?php echo $three_column_second_column_button_bg_color ?>-bg <?php echo $three_column_second_column_button_text_color; ?>-txt"<?php if($three_column_second_column_open_new_tab) : ?> target="_blank"<?php endif; ?>><?php echo $three_column_second_column_button_text; ?></a>
								</div>
								<?php endif; ?>
							</div>
							<div class="col-12 col-md-4 three-column-item-content<?php if (!($three_column_heading || $three_column_section_top_content)) : ?> no-margin-top<?php endif; ?>">
								<?php if ($three_column_third_column_image_url) : ?><img src="<?php echo $three_column_third_column_image_url ?>"><?php endif; ?>
								<h4><?php echo $three_column_third_column_heading; ?></h4>
								<div class="three-column-item-content-txt" data-mh="three-column-item-content-txt">
									<?php echo $three_column_third_column_content; ?>
								</div>
								<?php if ($three_column_third_column_show_button) : ?>
								<div class="three-column-item-content-bottom-btn">
									<a href="<?php echo $three_column_third_column_button_link; ?>" class="btn <?php echo $three_column_third_column_button_bg_color ?>-bg <?php echo $three_column_third_column_button_text_color; ?>-txt"<?php if($three_column_third_column_open_new_tab) : ?> target="_blank"<?php endif; ?>><?php echo $three_column_third_column_button_text; ?></a>
								</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="row">
							<div class="col-12 three-column-item-content-bottom">
								<div class="three-column-item-content-bottom-txt">
									<?php echo $three_column_section_bottom_content; ?>
								</div>
								<?php if ($three_column_show_button_end) : ?>
								<div class="three-column-item-content-bottom-btn">
									<a href="<?php echo $three_column_button_link; ?>" class="btn <?php echo $three_column_button_bg_color ?>-bg <?php echo $three_column_button_text_color; ?>-txt"<?php if($three_column_open_new_tab) : ?> target="_blank"<?php endif; ?>><?php echo $three_column_button_text; ?></a>
								</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php elseif ( get_row_layout() == 'grid_section' ) : ?>
			<?php
				$grid_section_bg_color = get_sub_field('grid_section_bg_color');
				$grid_section_text_color = get_sub_field('grid_section_text_color');
				$grid_section_heading = get_sub_field('grid_section_heading');
			?>
			<div class="grid-section layout<?php echo $count; ?><?php if($grid_section_bg_color) : ?> <?php echo $grid_section_bg_color; ?>-bg<?php endif; ?><?php if($grid_section_text_color) : ?> <?php echo $grid_section_text_color; ?>-txt<?php endif; ?>">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php if ($grid_section_heading) : ?>
							<h2><?php echo $grid_section_heading; ?></h2>
							<?php endif; ?>
						</div>
					</div>
					<div class="row">
						<?php if( have_rows('grid_section_grid_item') ): ?>
							<?php while ( have_rows('grid_section_grid_item') ) : the_row(); ?>
								<?php
								$grid_section_grid_item_bg_color = get_sub_field('grid_section_grid_item_bg_color');
								$grid_section_grid_item_text_color = get_sub_field('grid_section_grid_item_text_color');
								$grid_section_grid_item_image = get_sub_field('grid_section_grid_item_image');
								$grid_section_grid_item_image_url = wp_get_attachment_image_src($grid_section_grid_item_image, 'full')[0];
								$grid_section_grid_item_heading = get_sub_field('grid_section_grid_item_heading');
								$grid_section_grid_item_content = get_sub_field('grid_section_grid_item_content');
								$grid_section_grid_item_show_button = get_sub_field('grid_section_grid_item_show_button');
								if ($grid_section_grid_item_show_button) {
									$grid_section_grid_item_button_text = get_sub_field('grid_section_grid_item_button_text');
									$grid_section_grid_item_button_link = get_sub_field('grid_section_grid_item_button_link');
									$grid_section_grid_item_open_new_tab = get_sub_field('grid_section_grid_item_open_new_tab');
									$grid_section_grid_item_button_bg_color = get_sub_field('grid_section_grid_item_button_bg_color');
									$grid_section_grid_item_button_text_color = get_sub_field('grid_section_grid_item_button_text_color');
								}
								?>
							<div class="col-12 col-md-6">
								<div class="grid-section-item<?php if($grid_section_grid_item_bg_color) : ?> <?php echo $grid_section_grid_item_bg_color; ?>-bg<?php endif; ?><?php if($grid_section_grid_item_text_color) : ?> <?php echo $grid_section_grid_item_text_color; ?>-txt<?php endif; ?>" data-mh="grid-section-item">
									<div class="grid-section-item-image">
										<img src="<?php echo $grid_section_grid_item_image_url; ?>">
									</div>
									<div class="grid-section-item-content">
										<h4><?php echo $grid_section_grid_item_heading; ?></h4>
										<div class="grid-section-item-content-txt">
											<?php echo $grid_section_grid_item_content; ?>
										</div>
										<?php if ($grid_section_grid_item_show_button) : ?>
										<div class="three-column-item-content-bottom-btn">
											<a href="<?php echo $grid_section_grid_item_button_link; ?>" class="btn <?php echo $grid_section_grid_item_button_bg_color ?>-bg <?php echo $grid_section_grid_item_button_text_color; ?>-txt"<?php if($grid_section_grid_item_open_new_tab) : ?> target="_blank"<?php endif; ?>><?php echo $grid_section_grid_item_button_text; ?></a>
										</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php elseif ( get_row_layout() == 'heading' ) : ?>
			<?php
				$heading_bg_color = get_sub_field('heading_bg_color');
				$heading_text_color = get_sub_field('heading_text_color');
				$heading_text = get_sub_field('heading_text');
				$heading_icon = get_sub_field('heading_icon');
				$myair_icon = get_field('myair_icon', 'option');
				$myair_icon_url = wp_get_attachment_image_src($myair_icon,'full')[0];
				$mylights_icon = get_field('mylights_icon', 'option');
				$mylights_icon_url = wp_get_attachment_image_src($mylights_icon,'full')[0];
				$myplace_icon = get_field('myplace_icon', 'option');
				$myplace_icon_url = wp_get_attachment_image_src($myplace_icon,'full')[0];
				$myteam_icon = get_field('myteam_icon', 'option');
				$myteam_icon_url = wp_get_attachment_image_src($myteam_icon,'full')[0];
			?>
			<div class="heading-section layout<?php echo $count; ?>">
				<div class="heading-section-item<?php if($heading_bg_color) : ?> <?php echo $heading_bg_color; ?>-bg<?php endif; ?><?php if($heading_text_color) : ?> <?php echo $heading_text_color; ?>-txt<?php endif; ?>">
					<div class="container">
						<div class="row">
							<div class="col-<?php if ($heading_icon != 'none') : ?>9<?php else : ?>12<?php endif; ?> col-md-<?php if ($heading_icon != 'none') : ?>10<?php else : ?>12<?php endif; ?>">
								<h2><?php echo $heading_text; ?></h2>
							</div>
							<?php if ($heading_icon != 'none') : ?>
							<div class="col-3 col-md-2">
								<?php
								if ($heading_icon == 'myair') {
									echo "<img src='".$myair_icon_url."'>";
								} elseif ($heading_icon == 'mylights') {
									echo "<img src='".$mylights_icon_url."'>";
								} elseif ($heading_icon == 'myteam') {
									echo "<img src='".$myteam_icon_url."'>";
								} elseif ($heading_icon == 'myplace') {
									echo "<img src='".$myplace_icon_url."'>";
								}
								?>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		<?php elseif ( get_row_layout() == 'team_member' ) : ?>
			<?php
				$team_member_bg_color = get_sub_field('team_member_bg_color');
				$team_member_text_color = get_sub_field('team_member_text_color');
				$team_member_section_heading = get_sub_field('team_member_section_heading');
				$team_member_number = count(get_sub_field('team_member_item'));
			?>
			<div class="team-member-section layout<?php echo $count; ?><?php if($team_member_bg_color) : ?> <?php echo $team_member_bg_color; ?>-bg<?php endif; ?><?php if($team_member_text_color) : ?> <?php echo $team_member_text_color; ?>-txt<?php endif; ?>">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h2><?php echo $team_member_section_heading; ?></h2>
						</div>
					</div>
				</div>
				<?php if( have_rows('team_member_item') ): ?>
					<div class="slick-carousel team_member_slick">
					<?php while ( have_rows('team_member_item') ) : the_row(); ?>
					<?php
						$team_member_name = get_sub_field('team_member_name');
						$team_member_position = get_sub_field('team_member_position');
						$team_member_content = get_sub_field('team_member_content');
						$team_member_photo = get_sub_field('team_member_photo');
						$team_member_photo_url = wp_get_attachment_image_src($team_member_photo, 'full')[0];
						$team_member_photo_height = wp_get_attachment_image_src($team_member_photo, 'full')[2];
						$panel_height = $team_member_photo_height - 30;
						$team_member_item_bg_color = get_sub_field('team_member_item_bg_color');
						$team_member_item_text_color = get_sub_field('team_member_item_text_color');
					?>
					<div class="team-member-section-item<?php if($team_member_item_bg_color) : ?> <?php echo $team_member_item_bg_color; ?>-bg<?php endif; ?><?php if($team_member_item_text_color) : ?> <?php echo $team_member_item_text_color; ?>-txt<?php endif; ?>" style="height: <?php echo $panel_height; ?>px; margin-top: 30px;" data-panelheight="<?php echo $panel_height; ?>">
						<div class="container">
							<div class="row">
								<div class="col-6 col-md-6 image-panel">
									<div class="team-member-section-item-image">
										<img src="<?php echo $team_member_photo_url; ?>">
									</div>
								</div>
								<div class="col-6 col-md-6">
									<div class="team-member-section-item-content">
										<h3><?php echo $team_member_name; ?></h3>
										<div class="team-member-section-item-position">
											<?php echo $team_member_position; ?>
										</div>
										<div class="team-member-section-item-content-text">
											<?php if ($team_member_content) : ?><span>"</span><?php echo $team_member_content; ?><span>"</span><?php endif; ?>
										</div>
										<?php if($team_member_number > 1) : ?>
										<div class="team-member-section-item-content-nav slick-carousel-nav">
											<ul>
												<li class="nav-prev"><a href="#"><i class="fas fa-angle-left"></i></a></li>
												<li class="nav-next"><a href="#"><i class="fas fa-angle-right"></i></a></li>
											</ul>
										</div>
										<?php else : ?>
											<br><br>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
		<?php elseif ( get_row_layout() == 'testimonials' ) : ?>
			<?php
				$testimonials_bg_color = get_sub_field('testimonials_bg_color');
				$testimonials_text_color = get_sub_field('testimonials_text_color');
				$testimonials_section_heading = get_sub_field('testimonials_section_heading');
				$testimonials_number = count(get_sub_field('testimonials_item'));
			?>
			<div class="testimonials-section layout<?php echo $count; ?><?php if($testimonials_bg_color) : ?> <?php echo $testimonials_bg_color; ?>-bg<?php endif; ?><?php if($testimonials_text_color) : ?> <?php echo $testimonials_text_color; ?>-txt<?php endif; ?>">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h2><?php echo $testimonials_section_heading; ?></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<?php if( have_rows('testimonials_item') ): ?>
								<div class="testimonials-section-item slick-carousel testimonials_slick">
								<?php while ( have_rows('testimonials_item') ) : the_row(); ?>
									<?php
										$testimonial_name = get_sub_field('testimonial_name');
										$testimonial_location = get_sub_field('testimonial_location');
										$testimonial_content = get_sub_field('testimonial_content');
									?>
									<div class="testimonials-section-item-content">
										<div class="testimonials-section-item-content-text">
											<?php echo $testimonial_content;?>
										</div>
										<div class="testimonials-section-item-content-name">
											<?php echo $testimonial_name;?><?php if ($testimonial_location) : ?>, <?php echo $testimonial_location; ?><?php endif; ?>
										</div>
										<?php if($testimonials_number > 1) : ?>
										<div class="testimonials-section-item-content-nav slick-carousel-nav">
											<ul>
												<li class="nav-prev"><a href="#"><i class="fas fa-angle-left"></i></a></li>
												<li class="nav-next"><a href="#"><i class="fas fa-angle-right"></i></a></li>
											</ul>
										</div>
										<?php else : ?>
											<br><br>
										<?php endif; ?>
									</div>
								<?php endwhile; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<?php $count++; ?>
	<?php endwhile; ?>
<?php endif; ?>
</div>
