<?php
/**
 * The template part for displaying results in search pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class('search-result-item'); ?>>
	<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	<?php if ( 'post' == get_post_type() ) { ?>
		<?php get_template_part('inc/post','meta'); ?>
	<?php } ?>
	<?php the_excerpt(); ?>
</div><!-- /post -->
<div class="post-divider"></div>
