<?php if (function_exists('yoast_breadcrumb')) { ?>

    <div class="nav-breadcrumb hidden-xs">
    	<?php yoast_breadcrumb(); ?>
    </div><!-- /nav-breadcrumb -->

<?php } ?>

<div class="content-heading">

	<?php if(is_404()) { // If is 404 page ?>

        <h1 class="page-title">Error 404</h1>

	<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

	<?php } elseif (is_category()) { // If this is a category archive ?>

		<h1 class="page-title">Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category:</h1>

	<?php } elseif( is_tag() ) {  // If this is a tag archive ?>

		<h1 class="page-title">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>

	<?php } elseif (is_day()) { // If this is a daily archive ?>

		<h1 class="page-title">Archive for <?php the_time('F jS, Y'); ?>:</h1>

	<?php } elseif (is_month()) { // If this is a monthly archive ?>

		<h1 class="page-title">Archive for <?php the_time('F, Y'); ?>:</h1>

	<?php } elseif (is_year()) { // If this is a yearly archive ?>

		<h1 class="page-title">Archive for <?php the_time('Y'); ?>:</h1>

	<?php } elseif (is_author()) { // If this is an author archive ?>

		<h1 class="page-title">Author Archive</h1>

	<?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { // If this is a paged archive ?>

		<h1 class="page-title">Blog Archives</h1>

	<?php } elseif(is_home()) { // If is blog page?>

    	<?php
			$posts_page_id    = get_option( 'page_for_posts');
			$posts_page       = get_page( $posts_page_id);
			$posts_page_title = $posts_page->post_title;
			$posts_page_url   = get_page_uri($posts_page_id  );
		?>

        <h1 class="page-title"><?php echo $posts_page_title; ?></h1>

    <?php } elseif(is_search()) { // If is search page ?>

    	<h1 class="page-title">Search results for '<?php the_search_query(); ?>'</h1>

    <?php } else { // All other pages ?>

    	<h1 class="page-title"><?php echo get_the_title(); ?></h1>

    <?php } ?>

</div><!-- /content-heading -->
