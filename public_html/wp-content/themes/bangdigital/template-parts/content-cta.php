<?php if(get_field('sp_content_cta')) { ?>

	<div class="cta-content">
		
        <p><a href="<?php the_field('sp_book_button_link','options'); ?>" onClick="_gaq.push(['_trackEvent', 'Content CTA - <?php the_title(); ?>', '<?php the_field('sp_book_button_link','options'); ?>']);" class="block-link"><?php the_field('sp_content_cta'); ?></a></p>		
    
    </div><!-- /cta-content -->
    
<?php } ?>