<div class="sidebar sidebar-blog hidden-xs hidden-sm">

	<div class="sidebar-list">

    	<h3 class="sidebar-heading">View Latest Posts</h3>

    	<?php $my_query = new WP_Query('posts_per_page=5');?>

    	<ul class="ui-list">
			<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
			<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
			<?php endwhile;?>
		</ul>

		<?php wp_reset_query(); ?>

    </div><!-- /sidebar-list -->

</div><!-- /sidebar -->
