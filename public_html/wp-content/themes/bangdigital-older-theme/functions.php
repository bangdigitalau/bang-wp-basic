<?php
/**
 * Theme functions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 * 
 */
include_once 'includes/class-bang-theme-setup.php';