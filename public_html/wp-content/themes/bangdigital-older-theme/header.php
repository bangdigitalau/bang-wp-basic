<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <?php if (is_singular() && pings_open(get_queried_object())) : ?>
            <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php endif; ?>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div class="site-wrapper container-fluid">
            <div class="header row">
                <div class="col-md-2 site-logo">
                    <a href="<?php echo get_home_url(); ?>" title="<?php echo get_bloginfo('name')?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png"></a>
                </div>
                <div class="col-md-8 site-nav">
                    <?php
                    /**
                     * Main Menu
                     */
                    wp_nav_menu(array(
                        'theme_location' => 'main-menu',
                        'menu_class' => '',
                        'container' => ''
                    ));
                    ?>
                </div>
            </div>
