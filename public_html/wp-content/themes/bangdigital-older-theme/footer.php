<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<hr>

    <div class="row footer">
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>
