<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Bang_Ajax.
 * AJAX Event Handler.
 *
 */
class Bang_AJAX {

    /**
     * Hook in ajax handlers.
     */
    public static function init() {
        self::add_ajax_events();
    }

    /**
     * Hook in methods - uses WordPress ajax handlers (admin-ajax).
     */
    public static function add_ajax_events() {
        // bang_EVENT => nopriv
        $ajax_events = array(
        );

        foreach ($ajax_events as $ajax_event => $nopriv) {
            add_action('wp_ajax_bang_' . $ajax_event, array(__CLASS__, $ajax_event));
            if ($nopriv) {
                add_action('wp_ajax_nopriv_bang_' . $ajax_event, array(__CLASS__, $ajax_event));
            }
        }
    }
}

Bang_AJAX::init();
