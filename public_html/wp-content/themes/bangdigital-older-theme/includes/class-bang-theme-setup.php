<?php

/**
 * This is the class that init and setup all actions, 
 * filters and elements for the theme
 */
class Bang_Theme_Setup {

    /**
     * Constructor
     */
    function __construct() {
        add_action('init', array($this, 'init_actions'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
        add_action('widgets_init', array($this, 'register_sidebars_and_widgets'));
        add_action('after_setup_theme', array($this, 'theme_setup'));
        if (is_admin()) {
            $this->admin_includes();
            add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_scripts'));
        }
        $this->includes();
    }

    /**
     * Init Actions
     */
    public function init_actions() {
        $this->register_posts_types();
        $this->register_menus();
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page('Theme Settings');
        }

        //hide admin bar for all other users
        if (is_user_logged_in() && !current_user_can('delete_others_posts')) {
            add_filter('show_admin_bar', '__return_false');

            //redirect user to frontend if they trying to access wp-admin
            if (is_admin() && !( defined('DOING_AJAX') && DOING_AJAX )) {
                wp_redirect(home_url());
                exit;
            }
        }
    }

    /**
     * Include Admins
     */
    public function admin_includes() {
        
    }

    /**
     * Includes all files needed
     */
    public function includes() {
        include('class-bang-ajax.php');
        include('class-bang-shortcodes.php');
        include('class-bang-plugin-hooks.php');
    }

    /**
     * Enqueue Frontend Scripts and styles
     */
    public function enqueue_scripts() {
        wp_enqueue_style('bootstrap', get_template_directory_uri() . '/libs/bower_components/bootstrap/dist/css/bootstrap.min.css', array(), '3.3.7', 'all');
        wp_enqueue_style('slick', get_template_directory_uri() . '/libs/bower_components/slick-carousel/slick/slick.css', array(), '4.0.1', 'screen');
        wp_enqueue_style('fancybox', get_template_directory_uri() . '/libs/bower_components/fancybox/source/jquery.fancybox.css', array(), '2.1.5', 'screen');
        wp_enqueue_style('fontawesome', get_template_directory_uri() . '/libs/bower_components/font-awesome/css/font-awesome.min.css', array(), '2.1.5', 'screen');
        wp_enqueue_style('icofont', get_template_directory_uri() . '/libs/icofont/css/icofont.css', array(), '1..0.0', 'all');
        wp_enqueue_style('theme-style', get_template_directory_uri() . '/dist/style.min.css', array(), '1.0', 'screen');

        wp_enqueue_script('fancybox', get_template_directory_uri() . '/libs/bower_components/fancybox/source/jquery.fancybox.pack.js', array('jquery'), '2.1.5', true);
        wp_enqueue_script('jquery-slick', get_template_directory_uri() . '/libs/bower_components/slick-carousel/slick/slick.min.js', array('jquery'), '2.1.5', true);
        wp_enqueue_script('theme-script', get_template_directory_uri() . '/js/dist/functions.min.js', array('jquery', 'fancybox', 'jquery-slick'), '1.0.0', true);
    }

    /**
     * Enqueue admin scripts
     */
    public function enqueue_admin_scripts() {
        
    }

    /**
     * Register Custom Post Types and Taxonomies for the website
     */
    public function register_posts_types() {
        /**
         *    'property' => array(
         *    'single' => 'Property',
         *        'plural' => 'Properties',
         *        'args' => array()
         *    );
         * */
        $custom_post_types = array(
        );

        $defaults = array(
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => null,
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
        );

        foreach ($custom_post_types as $post_type => $post_type_data) {
            $single = $post_type_data['single'];
            $plural = $post_type_data['plural'];
            $labels = array(
                'name' => $plural,
                'singular_name' => $single,
                'menu_name' => $plural,
                'name_admin_bar' => $single,
                'add_new' => 'Add New',
                'add_new_item' => "Add New $single",
                'new_item' => "New $single",
                'edit_item' => "Edit $single",
                'view_item' => "View $single",
                'all_items' => "All $plural",
                'search_items' => "Search $plural",
                'parent_item_colon' => "Parent $plural",
                'not_found' => 'No ' . strtolower($single) . ' found',
                'not_found_in_trash' => 'No ' . strtolower($single) . ' found in Trash'
            );
            $args = $post_type_data['args'];
            $args = wp_parse_args($args, $defaults);
            $args['labels'] = $labels;
            register_post_type($post_type, $args);
        }
    }

    /**
     * Register Sidebars
     */
    function register_sidebars_and_widgets() {
        
    }

    /**
     * Register Menus
     */
    function register_menus() {
        register_nav_menus(
                array(
                    'main-menu' => __('Main Menu')
                )
        );
    }

    /**
     * Default Theme Setups
     */
    function theme_setup() {
        add_theme_support('post-thumbnails');
    }

}

new Bang_Theme_Setup();
