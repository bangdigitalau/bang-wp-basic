/**
 * Javascript for the Theme
 */
jQuery(function($) {
    /**
     * Our Theme Frontend JS
     */
    var Bang_Frontend_JS = {
        /**
         * Initialize variations actions
         */
        init: function() {
            this.theme_init();
            return this;
        },
        /***
         * Init Theme JS
         * @returns {undefined}
         */
        theme_init: function() {
        }
    };
    Bang_Frontend_JS.init();
});