<?php
/*
  Plugin Name: Bang Digital
  Description: Bang Digital - helper functions
  Version: 2.9.7
  Author: Bang Digital

  Changelog---
  --------------------
  2.9.7 - Gravity forms keyword blacklist - no longer case sensitive
  2.9.6 - re-added GA/GAQ toggle
  2.9.5 - removed GA/GAQ toggle. Added Gravity forms keyword blacklist
  2.9.4 - added <meta name="robots" content="noimageindex," "nofollow," "nosnippet"> for when site is on staging
  2.9.3 - GTM value added, updated labels so the fields make more sense
  2.9.2 - Update Wordfence settings token.
  2.9.1 - Remove the WordPress update nag
  2.9 - update wordfence settings token and add condition to make sure analytic codes exists before adding js
  2.8 - update wordfence settings token
  2.7 - Add new Event Tracking JS with Data Layer
  2.6 - Add WordFence Settings Import and Scaning function for our daily script to trigger scans
  2.5 - Put everything in a class, and disable xmlrpc and auto update.
  2.4 - Remove query string from static files
  2.3 - Analytics.js vs ga.js
  2.2 - Gravity forms 'after' submit hook for google conversions, added header/footer scripts, added GA tracking code input area
  2.1 - Push yoast SEO to bottom of page
  2.0 - Adding help doc toggle
  1.9 - Give editor access to Gravity Forms
  1.8 - Give editor access to menu
  1.7 - Custom WYSIWYG Stylesheet toggle
  1.6 - Disabled theme editor
  1.5 - Added toggle to bang login
  1.4 - Added theme options nad Google analtyics page
  1.3 - Added link to help.pdf, removed 'recent' from dashboard, re-instated local server message, removed buggy editor.css call (this needed to be called in the theme not the plugin), forced the dashboard to be 1 col
  1.2 - Added custom.css wysiwyg style
  1.1 - Added 'local server' message
 */

/**
 * Bang Digital Plugin
 */
Class Bang_Digital_Plugin {

    /**
     * Plugin Version
     * @var type
     */
    static $version = '2.9.2';

    /**
     * WordFence Settings
     */
    static $wordfence_settings_token = 'f714b23ee1f712514a9296870abc13f6d63ec4b18843069d88542f35f1125e2f24a2853cadc786e56a1cea037f68acd90733c87ec29b7c87052685df5c6b6836';

    /**
     * WordFence Scan Key
     * @var type
     */
    static $wordfence_scan_key = 'K3nLowfdwsfs1';


    /**
     * Initializing The plugin
     */
    static function init() {

        /**
         * Disable XMLRPC and Auto Updates
         */
        add_filter('xmlrpc_enabled', '__return_false');
        add_filter('automatic_updater_disabled', '__return_true');


        /**
         * Custom Behaviour Filters
         */
        add_filter('login_headerurl', array(__class__, 'bang_login_logo_url'));
        add_filter('login_errors', array(__class__, 'bang_login_error_message'), 10);
        add_filter('admin_footer_text', array(__class__, 'bang_admin_footer'), 10);
        add_filter('tiny_mce_before_init', array(__class__, 'unhide_kitchensink'));
        add_filter('wpseo_metabox_prio', array(__class__, 'lower_wpseo_metabox_prio'));
        add_filter('style_loader_src', array(__class__, 'remove_ver_query'));
        add_filter('script_loader_src', array(__class__, 'remove_ver_query'));
        add_action('admin_head', array(__class__, 'remove_core_update_notifications'));


        /**
         * WP-ADMIN Actions
         */
        add_action('init', array(__class__, 'add_scheduled_event'));
        add_action('wp_dashboard_setup', array(__class__, 'remove_dashboard_widgets'));
        add_action('login_head', array(__class__, 'bang_login_logo'));
        add_action('admin_menu', array(__class__, 'add_settings_menu'));
        add_action('admin_init', array(__class__, 'register_settings'));
        add_action('bang_import_wordfence_settings', array(__class__, 'import_wordfence_settings'));
        add_action('init', array(__class__, 'start_scan'), 4);



        /**
         * Frontend Actions
         */

        add_action('gform_validation', array(__class__, 'bang_custom_validation_blacklist_keywords'));
        add_action('wp_head', array(__class__, 'add_scripts_header'));
        add_action('wp_footer', array(__class__, 'add_scripts_footer'));
        add_action('wp_enqueue_scripts', array(__class__, 'enqueue_plugin_scripts_and_styles'));
        add_action('wp_head', array(__class__, 'add_robots_nofollow'));
    }

    /**
     * Enqueue Scripts and Styles
     */
    static function enqueue_plugin_scripts_and_styles() {
        /**
         * Event Tracking Code
         */
        if (get_option('ga_event_tracking')) {
            if (get_option('ga_event_tracking_data_layer')) {
                wp_enqueue_script('bang-event-tracking', plugins_url('assets/javascript/event-tracking-data-layer.js', __FILE__), array('jquery'), '1.0.0', true);
            } else {
                wp_enqueue_script('bang-event-tracking', plugins_url('assets/javascript/event-tracking.js', __FILE__), array('jquery'), '1.0.0', true);
            }
        }
    }

    /**
     * Bang Custom Logo on Login Page
     */
    static function bang_login_logo() {
        if (get_option('login_logo')) {
            echo '<style type="text/css">
            h1 a { background-image:url("http://clients.bangonline.com.au/wordpress-plugin/logo.png") !important; width:326px !important; height:142px !important; padding-bottom: 20px !important; background-size:auto !important }
            </style>';
        }
    }

    /**
     * Change login logo url to bang website
     * @return string
     */
    static function bang_login_logo_url($url) {
        return "http://www.bangonline.com.au/?utm_source=wordpress-login-screen&utm_medium=wordpress-login-screen&utm_campaign=wordpress-login-screen";
    }

    /**
     * Do not tell user which part of the login is wrong.
     * @param type $error
     * @return string
     */
    static function bang_login_error_message($error) {
        return 'The login information you have entered is incorrect. Please try again.';
    }

    /**
     * Add bang admin footer
     */
    static function bang_admin_footer() {
        echo '<span id="footer-thankyou">Need help? Contact Bang Digital at <a href="mailto:tech@bangdigital.com.au" target="_blank">tech@bangdigital.com.au</a> or 08 9328 7000</span>';
    }

    /**
     * Remove unused widgets for our client
     * @global type $wp_meta_boxes
     */
    static function remove_dashboard_widgets() {
        global $wp_meta_boxes;
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
        remove_meta_box('dashboard_activity', 'dashboard', 'normal');
    }

    /**
     * Unhide Kitchen sink so it always shows
     * @param array $args
     * @return boolean
     */
    static function unhide_kitchensink($args) {
        $args['wordpress_adv_hidden'] = false;
        return $args;
    }

    /**
     * Add Settings Menu
     */
    static function add_settings_menu() {
        //create new top-level menu
        add_menu_page('Bang Plugin Settings', 'Bang Digital', 'manage_options', 'bangdigital', array(__class__, 'settings_page'), plugins_url('/icon.png', __FILE__),3.1);
    }

    /**
     * Register Our Settings
     */
    static function register_settings() {
        register_setting('bang-plugin-settings-group', 'scripts_header');
        register_setting('bang-plugin-settings-group', 'scripts_footer');
        register_setting('bang-plugin-settings-group', 'google_analytics');
        register_setting('bang-plugin-settings-group', 'ga_event_tracking');
        register_setting('bang-plugin-settings-group', 'ga_event_tracking_data_layer');
        register_setting('bang-plugin-settings-group', 'google_tag_manager_id');
        register_setting('bang-plugin-settings-group', 'login_logo');
        register_setting('bang-plugin-settings-group', 'bang_editor_menu');
        register_setting('bang-plugin-settings-group', 'bang_editor_gravityform');
        register_setting('bang-plugin-settings-group', 'google_analytics_analyticsjs');
        register_setting('bang-plugin-settings-group', 'remove_query_strings');
        register_setting('bang-plugin-settings-group', 'google_analytics_track_login');
        register_setting('bang-plugin-settings-group', 'google_analytics_login_dimension');
        register_setting('bang-plugin-settings-group', 'gform_keyword_blacklist');
    }

    /**
     * Settings Page
     */
    static function settings_page() {
        ?>
        <style>
        .form-table{
            max-width:900px; margin:0 auto; border:1px solid #fff;
        }
        .form-table td,.form-table th{
            padding:1em;
            border-bottom:1px solid #fff;
        }
        .form-table label{
            font-weight:bold;
        }
        .form-table th{
            background:#eee;
            text-align:right;
        }
        .form-table textarea, .form-table input[type=text]{
            width:100%;
        }

        </style>
        <div class="wrap">
            <h1>Bang Digital</h1>
            <p>Plugin configuration</p>
            <form method="post" action="options.php">
                <?php settings_fields('bang-plugin-settings-group'); ?>
                <?php do_settings_sections('bang-plugin-settings-group'); ?>
                <table class="form-table" cellspacing="0" cellpadding="0" style="">

                    <tr valign="top">
                        <td colspan="2" align="center">
                        <h2>Google Analytics</h2>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><label for="google_analytics">Google Analytics Code</label><br /><small>UA-XXXXXX-X</small><p>If this is supplied then we will add the Google Analytics pageview tracker to all pages.</th>
                        <td>
                            <input type="text" name="google_analytics" value="<?php echo esc_attr(get_option('google_analytics')); ?>">
                            <p><em>Do not use this if you are using GTM to track pageviews.</em></p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">
                        <label for="ga_event_tracking">Track click events in Google Analytics?</label>
                        <p>This will add automatic Google Analytics event tracking to all <strong>email links, phone links, external links, pdf and other file downloads</strong></p>
                        </th>
                        <td>
                            <input type="checkbox" id="ga_event_tracking" name="ga_event_tracking" value="1" <?php checked(true, get_option('ga_event_tracking')); ?> />
                        </td>
                    </tr>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Use Analytics.js (universal) tracking?<br /><small>Untick if you want to use ga.js (which is never)</small></th>
                        <td>
                            <input type="checkbox" name="google_analytics_analyticsjs" <?php checked(true, get_option('google_analytics_analyticsjs')); ?>  value="1">
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Analytics Custom Dimension</th>
                        <td>
                            <input type="checkbox" name="google_analytics_track_login" <?php checked(true, get_option('google_analytics_track_login')); ?>  value="1">
                            <label for="google_analytics_track_login">Track If users are logged in or not</label>
                            <hr />
                             <input type="text" name="google_analytics_login_dimension" value="<?php echo get_option('google_analytics_login_dimension'); ?>">
                             What would you like the custom dimension to be called?<br /><small>Dimension to be used on tracking loggings, default is dimension1</small>
                        </td>
                    </tr>

                    <tr valign="top">
                        <td colspan="2" align="center">
                        <h2>Google Tag Manager</h2>
                        </td>
                    </tr>

                    <tr valign="top">
                        <th scope="row"><label for="google_tag_manager_id">Google Tag Manager Container ID</label><br /><small>GTM-XXX</small></th>
                        <td>
                            <input type="text" name="google_tag_manager_id" value="<?php echo esc_attr(get_option('google_tag_manager_id')); ?>">
                            <p>By entering a value here we will automatically embed the GTM code in to the theme. Note - <strong>it will not add pageview tracking - this needs to be set up in the GTM container</strong></p>
                        </td>
                    </tr>

                    <tr valign="top">
                        <th scope="row"><label for="ga_event_tracking_data_layer">Use GTM Datalayer event tracking</label><br /><small>Only activate this if you know what you're doing!</small></th>
                        <td>
                            <input type="checkbox" id="ga_event_tracking_data_layer" name="ga_event_tracking_data_layer" value="1" <?php checked(true, get_option('ga_event_tracking_data_layer')); ?> />
                            <p>If you choose this make sure you <strong>set up use GTM to track pageviews</strong>, and <strng><a href="http://wiki.bangonline.com.au/index.php?title=Google_Tag_Manager_-_Link_click_tracking" target="_blank">add the DataLayer triggers and events in GTM</a></strong> otherwise it will not work.</p>
                        </td>
                    </tr>

                     <tr valign="top">
                        <td colspan="2" align="center">
                        <h2>Code Snippets</h2>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row" style="width:40%">
                            <label for="scripts_header">Header Scripts</label><br />
                            <small>Please include the script tags</small></th>
                        <td>
                            <textarea type="text" name="scripts_header" rows="20" cols="50"><?php echo esc_attr(get_option('scripts_header')); ?></textarea>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><label for="scripts_footer">Footer Scripts</label><br /><small>Please include the script tags</small></th>
                        <td>
                            <textarea type="text" name="scripts_footer" rows="20" cols="50"><?php echo esc_attr(get_option('scripts_footer')); ?></textarea>
                        </td>
                    </tr>

                    <tr valign="top">
                        <td colspan="2" align="center">
                        <h2>Miscelaneous</h2>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><label for="login_logo">Show logo on login screen?</label></th>
                        <td>
                            <input type="checkbox" id="login_logo" name="login_logo" value="1" <?php checked(true, get_option('login_logo')); ?> />
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><label for="bang_editor_menu">Give editor users access to the menu?</label></th>
                        <td>
                            <input type="checkbox" id="bang_editor_menu" name="bang_editor_menu" value="1" <?php checked(true, get_option('bang_editor_menu')); ?> />
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><label for="bang_editor_gravityform">Give editor users access to gravity forms?</label></th>
                        <td>
                            <input type="checkbox" id="bang_editor_gravityform" name="bang_editor_gravityform" value="1" <?php checked(true, get_option('bang_editor_gravityform')); ?> />
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><label for="remove_query_strings">Remove query strings from scripts and css?</label></th>
                        <td>
                            <input type="checkbox" id="remove_query_strings" name="remove_query_strings" value="1" <?php checked(true, get_option('remove_query_strings')); ?>  />
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row" style="width:40%">
                            <label for="gform_keyword_blacklist">Gravity Form - Keyword blacklist</label><br />
                            <small>List keywords (comma separated), and we will block any form submissions which contain these values</small></th>
                        <td>
                            <textarea type="text" name="gform_keyword_blacklist" rows="5" cols="50"><?php echo esc_attr(get_option('gform_keyword_blacklist')); ?></textarea>
                        </td>
                    </tr>
                </table>
                <?php submit_button(); ?>
            </form>
        </div>
        <?php
    }

    /**
     * By deafult yoast set metabox to high priority, we want it to be lowww
     * @param type $high
     */
    static function lower_wpseo_metabox_prio($high) {
        return 'low';
    }

    /**
     * Add script header if any.
     * Also adds google analytic codes
     */
    static function add_scripts_header() {
        if (get_option('google_tag_manager_id')) {
            ?>
            <!-- Google Tag Manager -->
            <!-- INSTALLED BY THE BANG PLUGIN -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','<?php echo get_option("google_tag_manager_id"); ?>');</script>
            <!-- End Google Tag Manager -->
            <?php
        }

        if (get_option('scripts_header')) {
            echo get_option("scripts_header");
        }

        //dont have Google Analytics Code
        if (empty(get_option("google_analytics"))) {
            return;
        }

        if (get_option('google_analytics_analyticsjs')) :
            // - analytics.js
            ?>
            <script>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                ga('create', '<?php echo get_option("google_analytics") ?>', 'auto');
                ga('require', 'linkid', 'linkid.js');
                ga('require', 'displayfeatures');
            <?php
            /**
             * Track if users are logged in.
             */
            if (get_option('google_analytics_track_login')):
                $dimension = empty(get_option('google_analytics_login_dimension')) ? 'dimension1' : get_option('google_analytics_login_dimension');
                if (is_user_logged_in()):
                    echo "ga('set'," . "'" . $dimension . "'" . ", 'Logged in');\n";
                else:
                    echo "ga('set'," . "'" . $dimension . "'" . ", 'Not Logged in');\n";
                endif;
            endif;
            ?>
                ga('send', 'pageview');</script>
            <?php
        else:
            // - ga.js
            ?>
            <script type="text/javascript">
                var _gaq = _gaq || [];
                var pluginUrl =
                        '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
                _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
                _gaq.push(['_setAccount', '<?php echo get_option("google_analytics") ?>']);
                _gaq.push(['_trackPageview']);
                (function () {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();

            </script>
        <?php
        endif;
    }

    /**
     * Add scripts to footer if any
     */
    static function add_scripts_footer() {
        if (get_option('google_tag_manager_id')) {
        ?>
        <!-- Google Tag Manager (noscript) -->
        <!-- INSTALLED BY THE BANG PLUGIN -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo get_option("google_tag_manager_id"); ?>"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <?php
        }

        if (get_option('scripts_footer')) {
            echo get_option("scripts_footer");
        }
    }

    /**
     * Remove Version Query if requested
     */
    static function remove_ver_query($src) {
        if (get_option('remove_query_strings')) {
            $src = remove_query_arg('ver', $src);
        }
        return $src;
    }

    /**
     * Add Scheduled Events
     */
    static function add_scheduled_event() {
        if (!wp_next_scheduled('bang_import_wordfence_settings')) {
            //reset wordfence setting every day :)
            wp_schedule_event(time(), 'daily', 'bang_import_wordfence_settings');
            //run first time
            do_action('bang_import_wordfence_settings');
        }
    }

    /**
     * Import wordfence using the token.
     */
    static function import_wordfence_settings() {
        if (class_exists('wordfence')) {
            wordfence::importSettings(self::$wordfence_settings_token);

            /**
             * Disable WordFence Firewall? Not so sure about this yet
             */
            //wfWAF::getInstance()->getStorageEngine()->setConfig('wafStatus', 'disabled');
        }
    }

    /**
     * Run scan if the key is verified
     */
    static function start_scan() {
        if (isset($_GET['run_scan']) && $_GET['run_scan'] == 'true' && isset($_GET['scan_key']) && $_GET['scan_key'] == self::$wordfence_scan_key) {
            //valid request, start scan
            if (class_exists('wordfence')) {
                wordfence::startScan();
            }
        }
    }

    /**
     * Remove the WordPress update nag
     * @return type
     */
    static function remove_core_update_notifications() {
        if (!current_user_can('update_core')) {
            remove_action( 'admin_notices', 'update_nag', 3 );
        }
    }

    /**
     * Add robots nofollow on staging sites
     */
    function add_robots_nofollow() {
        $current_urL = $_SERVER['SERVER_NAME'];
        if(strpos($current_urL,'staging.bangdigital.com.au') !== false || strpos($current_urL,'localhost.com') !== false){
            echo '<meta name="robots" content="noindex,noimageindex, nofollow, nosnippet">';
        }

    }

    /**
     *  Keyword blacklist for Gravity Forms
     */
    function bang_custom_validation_blacklist_keywords($validation_result){

        $gform_keyword_blacklist = get_option("gform_keyword_blacklist");

        if($gform_keyword_blacklist){

            $form = $validation_result['form'];

            // Grab Keywords

            $stop_words = explode(',', strtolower($gform_keyword_blacklist));
            $stop_id = array();


            foreach($_POST as $id => $post){
                $post = strtolower($post);

                if(!is_array($stop_words)) $stop_words = array($stop_words);

                foreach($stop_words as $what) {
                    // flatten any fields that store data as array
                    if(is_array($post)){
                        $post = implode(" ",$post);
                    }
                    if(($pos = strpos($post, $what))!==false){
                        $stop_id[] = $id;
                    }
                }
            }


            if(sizeof($stop_id) > 0){
                $validation_result['is_valid'] = false;
                foreach($form['fields'] as &$field){
                    foreach($stop_id as $id){
                        $the_id = (int) str_replace('input_', '', $id);
                        if($field['id'] == $the_id){
                            $field['failed_validation'] = true;
                            $field['validation_message'] = 'Invalid '.$error;
                        }
                    }
                }
            }

            //Assign modified $form object back to the validation result
            $validation_result["form"] = $form;
            return $validation_result;
        }else{
            return $validation_result;
        }
    }


}

Bang_Digital_Plugin::init();
