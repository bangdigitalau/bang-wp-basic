/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(document).ready(function($) {
if (!(!(window._gaq === undefined || window.ga === undefined) || window.jQuery === undefined)){(function(e){"use strict"; e(function(){var t = "\\.(" + "dmg|exe|" + "rar|zip|" + "pdf|txt|" + "mp3|" + "avi|flv|mov|wav|wma|wmv|" + "docx?|xlsx?|pptx?" + ")$", n = {download:new RegExp(t, "i"), email:/^mailto\:/i, external:/^https?\:\/\//i, hostName:new RegExp(location.host, "i"), phone:/^tel\:/i}, r = e("base").attr("href") || "", i = function(e){return n.download.test(e)}, s = function(e){return n.email.test(e)}, o = function(e, t){return(t || "").indexOf("external") !== - 1 || n.external.test(e) && !n.hostName.test(e)}, u = function(e){return n.phone.test(e)}, a = function(e){var t = e.split("/"); return t[t.length - 1]}, f = function(e){return e.match(n.download)[1].toUpperCase()}, l = function(e, t, n, r){e = e || ""; t = t || "Link clicks"; n = n || ""; r = r || ""; return{category:t, action:n, label:r, loc:e}}; e("body").on("click", "a", function(){var t = e(this), c = t.attr("href") || "", h = t.attr("rel") || "", p = (t.attr("target") || "").toLowerCase(), d, v; if (o(c, h)){v = l(c, null, "External link", c)} else if (i(c)){d = a(c); v = l(r + c, "Downloads", f(d) + " download", d)} else if (s(c)){v = l(c, null, "Email link", c.replace(n.email, ""))} else if (u(c)){v = l(c, null, "Telephone link", c.replace(n.phone, ""))} else{return}if (!(window._gaq === undefined)){_gaq.push(["_trackEvent", v.category, v.action, v.label])}if (!(window.ga === undefined)){ga('send', 'event', v.category, v.action, v.label)}})})})(jQuery)}
});